# INSTALL_NIKITLY


### ⬇️Установка

клонировать репозиторий:
```
git clone https://gitlab.com/CheXnik/testify.git
```
перейти в папку "nikitly":
```
cd testify
```

### ⬇️Установить docker:


```
sudo apt update -y
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" -y
sudo apt update -y
apt-cache policy docker-ce
sudo apt install docker-ce -y
sudo usermod -aG docker ${USER}
su - ${USER}
id -nG
sudo usermod -aG docker ubuntu
```

### ⬇️Установить docker-compose:

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

### 🛠Настроить среду


📥Создать файл ".env" 
```
touch .env
```

И заполните файл как в примере ".env.example", но с вашими собственными настройками. Где «xxxx» вам нужно изменить в настройках (используйте «nano .env» для редактирования этого файла)


### 🕹Запуск приложение

🕹Запуск:

```
docker-compose up
```

🌅Вам нужно собрать статические файлы:
```
docker-compose exec backend python manage.py collectstatic
```
🏞И медия файлы:
```
docker-compose exec backend cp -r media/ /var/www/testify/media
```

🗄Вам нужно создать базу данных:
```
docker-compose exec postgresql bash
```
⏩Следующий шаг:
```
su - postgres
```
⏩Следующий шаг:
```
psql
```
⏩Следующий шаг (вы можете создать своего пользователя, изменить пароль и другие данные):
```
CREATE DATABASE testify; 
CREATE USER testify_admin WITH PASSWORD 'xxxx';
ALTER ROLE testify_admin SET client_encoding TO 'utf8';
ALTER ROLE testify_admin SET default_transaction_isolation TO 'read committed';
ALTER ROLE testify_admin SET timezone TO 'UTC';
GRANT ALL PRIVILEGES ON DATABASE testify TO testify_admin;
ALTER USER testify_admin CREATEDB;

```

Вернуться в папку "testify" (использовать комбинацию клавиш "ctrl + d" 3 раза)

Вам необходимо добавить измененные данные в файл ".env" (используйте nano .env)

Далее сделайте миграцию:
```
docker-compose exec backend python manage.py migrate
```

### ✅Конец

Поздравляю! Теперь у вас есть собственный сайт, осталось сделать доменное имя, и завоёвывать интернет.
