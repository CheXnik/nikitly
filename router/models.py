import random

from django.db import models

class Route(models.Model):
    original_url = models.URLField(help_text= "Добавьте исходный URL-адрес, который вы хотите сократить.")
    key = models.CharField(null=False, blank=True, default=random.seed(26), max_length=128, unique= True, help_text= "Добавьте любые символы, которые будут использоваться в качестве ключа после домена сайта. Максимальная длина 128 символов.")

    def __str__(self):
        return f"{self.key}"
