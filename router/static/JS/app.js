VANTA.BIRDS ({
  el: "#intro",
  mouseControls: true,
  touchControls: true,
  gyroControls: false,
  minHeight: 200.00,
  minWidth: 200.00,
  scale: 0.50,
  scaleMobile: 1.00,
  color1: 0xc300ff
})