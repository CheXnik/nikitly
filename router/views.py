from random import random

from django.contrib import messages
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic import ListView

from .forms import RouterForm
from .models import Route

def home(request):
    BASE_URL = request.get_raw_uri()
    form = RouterForm(request.POST or None)
    if form.is_valid():
        key = form.cleaned_data.get('key')
        form.save()
        messages.success(request, f"Сокращенный URL-адрес {BASE_URL}{key}")
        return redirect('home')
    return render(request, 'home.html', {"form": form})

def how_to(request):
    return render(request, 'how_to_use.html')

class URLListView(ListView):
    model = Route
    context_object_name = 'urls'
    paginate_by = 10

def redirector(request, key):
    instance = get_object_or_404(Route, key= key)
    return redirect(instance.original_url)
