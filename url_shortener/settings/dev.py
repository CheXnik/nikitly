from url_shortener.settings.components.base import * # noqa
from url_shortener.settings.components.database import * # noqa
from url_shortener.settings.components.dev_tools import * # noqa
from url_shortener.settings.components.email import * # noqa
from url_shortener.settings.components.celery import * # noqa
from url_shortener.settings.components.rest import * # noqa


DEBUG = True
ALLOWED_HOSTS = ['*']
