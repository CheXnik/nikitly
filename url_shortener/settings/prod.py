import os # noqa

from url_shortener.settings.components.base import * # noqa
from url_shortener.settings.components.database import * # noqa
from url_shortener.settings.components.email import * # noqa
from url_shortener.settings.components.celery import * # noqa
from url_shortener.settings.components.rest import * # noqa


DEBUG = False

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', '').split(':')

STATIC_ROOT = '/var/www/nikitly/static'

MEDIA_ROOT = '/var/www/nikitly/media'
