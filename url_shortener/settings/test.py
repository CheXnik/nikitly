from url_shortener.settings.components.base import * # noqa
from url_shortener.settings.components.dev_tools import * # noqa


DEBUG = True
ALLOWED_HOSTS = ['*']
