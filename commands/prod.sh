#!/bin/bash

gunicorn -w ${WORKERS} -b 0:${PORT} url_shortener.wsgi:application --log-level=${LOG_LEVEL}
