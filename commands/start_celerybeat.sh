#!/bin/bash

rm /tmp/celerybeat-schedule /tmp/celerybeat.pid
celery -A url_shortener beat -l info --schedule=/tmp/celerybeat-schedule --pidfile=/tmp/celerybeat.pid
